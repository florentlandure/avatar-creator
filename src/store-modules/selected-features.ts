import { IFeatureParams, ISelectedFeatures } from '@/models';

export const selectedFeaturesModule = {
  state: {
    gender: 0,
    shirtColor: 0,
    eyes: {
      shape: 0,
      color: 0,
      size: 0,
    },
    eyebrows: {
      shape: 0,
      color: 0,
      size: 0,
    },
    mouth: {
      shape: 0,
      color: 0,
      size: 0,
    },
    face: {
      shape: 0,
      color: 0,
    },
    facialHair: {
      shape: 0,
      color: 0,
    },
    hair: {
      shape: 0,
      color: 0,
    },
    ears: {
      shape: 0,
      size: 0,
    },
  },
  mutations: {
    selectFeature(
      state: ISelectedFeatures,
      payload: { [key: string]: IFeatureParams | number },
    ) {
      const key = Object.keys(payload)[0];

      if (typeof payload[key] === 'number') {
        state[key] = payload[key];
      } else {
        state = {
          ...state,
          ...payload,
        };
      }
    },
  },
  actions: {
    selectFeature(
      context: any,
      payload: { [key: string]: IFeatureParams | number },
    ) {
      context.commit('selectFeature', payload);
    },
  },
};
