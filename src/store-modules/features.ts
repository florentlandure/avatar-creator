export const featuresModule = {
  state: {
    gender: ['male', 'female'],
    shirtColor: ['black', 'white', 'blue'],
    eyes: {
      shape: [0, 1, 2],
      color: [0, 1, 2],
      size: [0, 1, 2],
    },
    eyebrows: {
      shape: [0, 1, 2],
      color: [0, 1, 2],
      size: [0, 1, 2],
    },
    mouth: {
      shape: [0, 1, 2],
      color: [0, 1, 2],
      size: [0, 1, 2],
    },
    face: {
      shape: [0, 1, 2],
      color: [0, 1, 2],
    },
    facialHair: {
      shape: [0, 1, 2],
      color: [0, 1, 2],
    },
    hair: {
      shape: [0, 1, 2],
      color: [0, 1, 2],
    },
    ears: {
      shape: [0, 1, 2],
      size: [0, 1, 2],
    },
  },
  mutations: {},
  actions: {},
};
