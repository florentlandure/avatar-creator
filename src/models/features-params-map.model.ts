export interface ParamsMapItem {
  id: string;
  name: string;
  value: number;
}

export type ParamsMap = ParamsMapItem[];
