export interface ISelectedFeatures {
  gender: number;
  shirtColor: number;
  eyes: IFeatureParams;
  eyebrows: IFeatureParams;
  mouth: IFeatureParams;
  face: IFeatureParams;
  facialHair: IFeatureParams;
  hair: IFeatureParams;
  ears: IFeatureParams;
  [key: string]: number | IFeatureParams;
}

export interface IFeatureParams {
  shape?: number;
  color?: number;
  size?: number;
}

export interface IAvailableFeaturesMap {
  gender: string[];
  shirtColor: string[];
  eyes: IFeatureParams[];
  eyebrows: IFeatureParams[];
  mouth: IFeatureParams[];
  face: IFeatureParams[];
  facialHair: IFeatureParams[];
  hair: IFeatureParams[];
  ears: IFeatureParams[];
}
