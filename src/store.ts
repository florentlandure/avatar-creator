import Vue from 'vue';
import Vuex from 'vuex';
import { selectedFeaturesModule, featuresModule } from './store-modules';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    selectedFeatures: selectedFeaturesModule,
    features: featuresModule,
  },
});
